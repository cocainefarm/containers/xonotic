#!/bin/bash

function install_xonotic {
  wget -q https://dl.xonotic.org/xonotic-${XONOTIC_VERSION:=0.8.2}.zip -O xonotic.zip
  aunpack -q xonotic.zip
  mv /Xonotic/* /xonotic
  rm -rf xonotic-${XONOTIC_VERSION:=0.8.2}.zip /Xonotic
  touch /xonotic/installed
  printf "rcon_password \"changeme\"" >> /xonotic/server/server.cfg
}

function start_xonotic {
  cd /xonotic
  /xonotic/xonotic-linux64-dedicated -sessionid 1
}

function stop_xonotic {
  pkill xonotic-linux64-dedicated
}

if [[ $1 == "install" ]]; then
  install_xonotic
elif [[ $1 == "start" ]]; then
  start_xonotic
elif [[ $1 == "stop" ]]; then
  stop_xonotic
elif [ ! -f "/xonotic/installed" ]; then
  install_xonotic
  start_xonotic
else
  start_xonotic
fi
  
